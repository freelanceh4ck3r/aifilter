# AIFilter

AI (Genetic algorithm + neural network) content filter for postfix.
Just an experiment, nothing serious.

## Teaching the filter

You will need two maildirs. One for the HAM and one for the SPAM.

Use extract.py for feature extraction:
```
./extract.py ham_folder 0 >learn.txt
./extract.py spam_folter 1 >>learn.txt
```

You might delete the last empty line from learn.txt

After that, run the training PHP script:

```
./neuralspam NEURON=5,LAYER=1,FPRATE=0.01,FNRATE=0.1
```

NEURON: number of neurons per hidden layer
LAYER: number of hidden layers
FPRATE: acceptable false positive rate
FNRATE: acceptable false negative rate

If the training process is done, you will see an array-dump in the output.
Replace variable "w" in ANNFilter.py (around line 110).

You will have to modify it a bit (adding two np.array() functions)

## Postfix config

Modify master.cf and add a pre-queue filter:

```
smtp      inet  n       -       y       -       -       smtpd
    -o smtpd_proxy_filter=inet:[127.0.0.1]:10021
    -o smtpd_recipient_restrictions=permit_mynetworks,reject
    -o receive_override_options=no_address_mappings
```

## User setup

Create a user named aifilter, and set the correct access rights:

```
sudo useradd aifilter

sudo chown -R aifilter:aifilter /usr/local/aifilter
sudo chmod +x /usr/local/aifilter/AIFilter.py
```

Run the service with
```
sudo service aifilter start
```

