#!/usr/bin/python3

import sys
import email
from email.header import Header
import re
import os
from ANN import ANN
import numpy as np
import smtplib
import ssl

class ANNFilter:
    def __init__(self):
        pass

    def getdomain(self, s, ismail=True):
        try:
            if ismail:
                tmp = s.split('@')[-1]
            else:
                tmp = s.split('/')[-1]
            dom = tmp.split(".")[-2]+"."+tmp.split(".")[-1]
        except:
            return ""
        return dom.split(">")[0]

    def getfield(self, msg, field):
        tmp = ""
        try:
            tmp = msg[field]
        except:
            pass
        return tmp

    
    def unicode_it(self, data):
        # Take a string of data and convert it to unicode
        try:
            return data #unicode(data, errors="replace").strip()
        except TypeError as E:
            return u""

    def extract(self, msg, msg_obj, attachments):
        if msg.is_multipart():
            #print("MULTIPART")
            for part in msg.get_payload():
                if part.is_multipart():
                    self.extract(part, msg_obj, attachments)
                    continue
                if part.get('Content-Disposition') is None:
                    msg_obj["body"] += self.unicode_it(part.get_payload())
                else:
                    if part.get('Content-Disposition').startswith('attachment'):
                        attachments[part.get_filename()] = {
                                'data': part.get_payload(decode=True),
                                'mime': part.get_content_type()
                            }
        else:
            msg_obj["body"] += self.unicode_it(msg.get_payload())
        return

    def filter(self, msg_raw, id):
        try:
            msg_raw = email.message_from_bytes(msg_raw)
        except Exception as e:
            print('Could not create mail object from bytes... Do it from string');
            print(e)
            msg_raw = email.message_from_string(msg_raw)

        msg = {"path": "stdin", "body": ""}
        attachments = {}
        for key, value in msg_raw.items():
            msg[key.lower()] = self.unicode_it(value)
        lists = ["from", "cc", "bcc", "to"]
        self.extract(msg_raw, msg, attachments)
        
        print("New mail; {} -> {}".format(self.getfield(msg,'from'),self.getfield(msg,'to')))
        #print(msg)
        rec = {}
        fromdom = self.getdomain(self.getfield(msg,'from'))
        #retpath = self.getdomain(self.getfield(msg, 'return-path'))

        #print("Return path [{}], From [{}]".format(retpath,self.getdomain(self.getfield(msg, 'from'))))

        #rec["SENDER"] = 1 if retpath == self.getdomain(self.getfield(msg, 'from')) else 0
        rec['PRECEDENCE'] = 1 if self.getfield(msg, 'precedence') != "" else 0
        #rec["RECV"] = 1 if self.getdomain(self.getfield(msg, 'to')) == self.getdomain(self.getfield(msg, 'delivered-to')) else 0
        rec['SIZE'] = len(msg['body']) / 150000

        rec['AGENT'] = 1 if self.getfield(msg, 'user-agent') != "" else 0
        rec['PHP'] = 1 if self.getfield(msg, 'x-php-originating-script') != "" else 0
        rec['RECPTID'] = 1 if self.getfield(msg, 'x-mailer-recptid') != "" else 0

        tabs = re.findall("(=09)", msg['body'])
        rec['TABS'] = len(tabs)/2000;

        rec['COMPLAINTS'] = 1 if self.getfield(msg, 'x-complaints-to') != "" else 0

        try:
            body=quopri.decodestring(msg['body']).decode()
        except Exception as e:
            body=msg['body']

        links = re.findall("(?P<url>https?://[^\s/?]+)", body)
        rec['LINKS'] = len(links) / 50
        rec['EXTLINKS'] = 0
        for l in links:
            if self.getdomain(l, False) != fromdom:
                rec['EXTLINKS'] += 1

        rec["EXTLINKS"] /= 50

        for key in ['SIZE','LINKS','EXTLINKS','TABS']:
            if rec[key] > 1:
                rec[key] = 1

        w=[np.array([[-36.509259999999976,3.395600000000001,-11.805320000000005,16.40235999999999,7.309780000000021,-2.121519999999998,-33.94248999999998,14.73793000000001,7.461300000000004],
            [-3.758939999999998,5.262059999999983,-8.369110000000001,9.688019999999993,2.5879499999999984,-12.277130000000003,23.43923,29.028859999999995,4.282090000000001],
            [6.748999999999991,-6.398500000000005,3.7614599999999956,-13.549590000000018,0.45971999999999896,-8.310719999999995,-18.34651,-14.685909999999994,-10.267960000000002],
            [-10.20679999999999,11.147370000000008,-4.5234399999999955,24.03997999999998,18.124869999999994,-21.53975999999999,17.755230000000005,-7.660950000000011,-2.093199999999998],
            [7.149590000000008,-20.69670000000001,-14.721740000000004,-16.399410000000024,23.908660000000015,12.77607000000001,2.0102899999999932,18.149059999999988,-7.990090000000002]]),
        np.array([[-17.07043999999997,21.513190000000023,8.411840000000012,-12.633650000000003,-11.155259999999998]])]

#            w=[np.array([[3.447669999999999,-14.167700000000007,-3.217769999999999,-25.101710000000015,-0.1340700000000039,5.683610000000005],
#               [-6.200490000000011,7.739420000000005,-17.078820000000007,7.85891,1.3590099999999985,-28.51844999999998],
#                [-3.764830000000007,1.0183299999999933,-35.83890000000003,22.46644000000001,13.297389999999979,-23.194649999999967],
#                [13.940499999999995,16.780270000000005,-9.91934,3.1652899999999837,-9.147829999999999,5.383260000000019],
#                [23.57624000000002,-0.1982299999999942,-16.5372,-21.99420999999998,-22.974639999999994,-12.745120000000009]]),
#            np.array([[-14.17004999999997,-25.248260000000013,-16.56070999999999,10.189770000000003,-8.539290000000001]])]

        ann = ANN(9,1,6,1)
        ann.setw(w)
        input=np.array([[rec["PRECEDENCE"]], [rec["SIZE"]], [rec["LINKS"]], [rec["EXTLINKS"]], [rec["AGENT"]], [rec["PHP"]], [rec["RECPTID"]], [rec["TABS"]], [rec["COMPLAINTS"]]])
        rate = ann.calc(input)[0][0]
        decision="HAM"
        if rate > 0.8:
            decision="SPAM"
            text,enc = email.header.decode_header(msg_raw['Subject'])[0]
            #print(text)
            encodedSubj = False
            try:
                text = text.decode(enc)
                encodedSubj = True
            except Exception as e:
                pass
            subj = Header("!AISPAM! "+text,'utf-8')
            del msg_raw['Subject']
            msg_raw['Subject'] = subj
            #print("SUBJ ["+str(msg_raw['Subject'])+"]")

        header = "{} score {:.2f}% - PRECEDENCE[{}], SIZE[{}], LINKS[{}], EXTLINKS[{}], AGENT[{}], PHP[{}], RECPTID[{}], TABS[{}], COMPLAINTS[{}]"

        msg_raw["X-AIFilter"] = header.format(decision, (rate*10000)/100, rec["PRECEDENCE"], rec["SIZE"], rec["LINKS"], rec["EXTLINKS"], rec["AGENT"], rec["PHP"], rec["RECPTID"], rec["TABS"], rec["COMPLAINTS"])

        s=smtplib.SMTP("localhost",10026)

        try:
            file = open("/dev/shm/"+str(os.getpid())+"_"+str(id), "wb")
            mystr = msg_raw.as_string()
            file.write(bytearray(mystr,'utf-8'))
            file.close()
        except:
            print("Could not write mail to file...")

        try:
            s.send_message(msg_raw)
        except Exception as e:
            print("Could not send message")
            print(e)

        s.quit()


