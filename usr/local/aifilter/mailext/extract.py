#!/usr/bin/python3

import mailbox
import email
import sys
import re
import quopri

def unicode_it(data):
    # Take a string of data and convert it to unicode
    try:
        return data #unicode(data, errors="replace").strip()
    except TypeError as E:
        return u""

def extract(msg, msg_obj, attachments):
    if msg.is_multipart():
        for part in msg.get_payload():
            if part.is_multipart():
                extract(part, msg_obj, attachments)
                continue
            if part.get('Content-Disposition') is None:
                msg_obj["body"] += unicode_it(part.get_payload())
            else:
                if part.get('Content-Disposition').startswith('attachment'):
                    attachments[part.get_filename()] = {
                            'data': part.get_payload(decode=True),
                            'mime': part.get_content_type()
                        }
    else:
        msg_obj["body"] += unicode_it(msg.get_payload())
    return

def getdomain(s, ismail=True):
    try:
        if ismail:
            tmp = s.split('@')[-1]
        else:
            tmp = s.split('/')[-1]
        dom = tmp.split(".")[-2]+"."+tmp.split(".")[-1]
    except:
        return ""
    return dom.split(">")[0]

def getfield(msg, field):
    tmp = ""
    try:
        tmp = msg[field]
    except:
        pass
    return tmp


#print("Folder: ",sys.argv[1])
mail_directory =  sys.argv[1]

words = {}

mdir = mailbox.Maildir(mail_directory) 
for directory in mdir.list_folders():
    d = mdir.get_folder(directory)
    for fname,msg in d.iteritems():
        msg_obj = {"path": directory, "body": ""}
        attachments = {}
        for key, value in msg.items():
            msg_obj[key.lower()] = unicode_it(value)
        lists = ["from", "cc", "bcc", "to"]
        extract(msg, msg_obj, attachments)
        #print(msg_obj)
        #exit(0)
        rec = {}
        fromdom = getdomain(getfield(msg_obj,'from'))
        #retpath = getdomain(getfield(msg_obj, 'return-path'))
        #rec["SENDER"] = 1 if retpath == getdomain(getfield(msg_obj, 'from')) else 0
        rec['PRECEDENCE'] = 1 if getfield(msg_obj, 'precedence') != "" else 0
        #rec["RECV"] = 1 if getdomain(getfield(msg_obj, 'to')) == getdomain(getfield(msg_obj, 'delivered-to')) else 0
        rec['SIZE'] = len(msg_obj['body']) / 150000

        rec['AGENT'] = 1 if getfield(msg_obj, 'user-agent') != "" else 0
        rec['PHP'] = 1 if getfield(msg_obj, 'x-php-originating-script') != "" else 0
        rec['RECPTID'] = 1 if getfield(msg_obj, 'x-mailer-recptid') != "" else 0

        tabs = re.findall("(=09)", msg_obj['body'])
        try:
            body=quopri.decodestring(msg_obj['body']).decode()
        except Exception as e:
            body=msg_obj['body']
        links = re.findall("(?P<url>https?://[^\s/?]+)", body)
        #print(body)
        rec['TABS'] = len(tabs)/2000;

        rec['COMPLAINTS'] = 1 if getfield(msg_obj, 'x-complaints-to') != "" else 0

        rec['LINKS'] = len(links) / 50
        rec['EXTLINKS'] = 0
        for l in links:
            if getdomain(l, False) != fromdom:
                rec['EXTLINKS'] += 1

        rec["EXTLINKS"] /= 50

        for key in ['SIZE','LINKS','EXTLINKS','TABS']:
            if rec[key] > 1:
                rec[key] = 1

        #print("%f %f %f %f %f %f %f" % (rec["SENDER"], rec["RECV"], rec["PRECEDENCE"], rec["SIZE"], rec["LINKS"], rec["EXTLINKS"], float(sys.argv[2])))
        print("%f %f %f %f %f %f %f %f %f %d %s" % (rec["PRECEDENCE"], rec["SIZE"], rec["LINKS"], rec["EXTLINKS"], rec["AGENT"], rec["PHP"], rec["RECPTID"], rec["TABS"], rec["COMPLAINTS"], float(sys.argv[2]), fname))
