#!/usr/bin/php
<?php
include("ANN.php");

function sortFunc($a, $b) {
    if ($a["score"] == $b["score"]) return 0;
    return ($a["score"] < $b["score"]) ? -1 : 1;
}

$totalspam = 0;
$totalham = 0;

foreach (explode("\n", file_get_contents("learn.txt")) as $rec) {
    $val = explode(" ", $rec);
    if (!defined("INPUTS")) {
        define("INPUTS", count($val) - 2);
        echo "Number of inputs: ".INPUTS."\n";
    }
    foreach ($val as $k => $v) $tmp[$k] = floatval($v);

    $tmpinp = [];
    for ($i=0; $i<INPUTS; $i++) {
        $tmpinp[] = [$tmp[$i]];
    }
    $ti[] = $tmpinp; //, [$tmp[4]], [$tmp[5]]];
    $to[] = [[$tmp[INPUTS]]];

    //var_dump($ti[0]);
    //var_dump($to[0]);
    //exit(0);
    //if ($tmp[6] > 0.5) 
    if ($tmp[INPUTS] > 0.5) 
        $totalspam++;
    else
        $totalham++;
}

$ann = [];
$score = [];

if (isset ($argv[1])) {
    foreach (explode(",", $argv[1]) as $p) {
        $tmp = explode("=",$p);
        echo "Set ".$tmp[0]." = ".$tmp[1]."\n";
        define($tmp[0],$tmp[1]);
    }
}
if (!defined("LAYER")) define ("LAYER", 2);
if (!defined("NEURON")) define ("NEURON", 4);
if (!defined("POPULATION")) define ("POPULATION", 100);
if (!defined("FPRATE")) define("FPRATE", 0.05);
if (!defined("FNRATE")) define("FNRATE", 0.1);
$start = microtime(true);

for ($i=0; $i<POPULATION; $i++) {
    $ann[] = new ANN(INPUTS, LAYER,NEURON,1);
    $ann[count($ann)-1]->random();
    $ann[count($ann)-1]->setRules($ti, $to);
}

$round=0;

while (true) {
    $round += 1;
    $score = [];
    for ($i=0; $i<count($ann); $i++) {
        echo $i."                \r";
        $res = $ann[$i]->test();
        $score[] = array('ann' => $ann[$i], 'score' => $res);
    }
    
    usort($score, "sortFunc");
    
    if ($round % 1 == 0) {
        ob_start();
        system('clear');
        print("Round #".$round." - ".$score[0]["score"]."\n");
        for ($i=0; $i<count($ti); $i+=500) {
            print(number_format($score[0]['ann']->calc($ti[$i])[0][0],2)." / ".number_format($to[$i][0][0],2)."\t");
        }
        print("\n");

        $falsepos = 0;
        $falseneg = 0;
        for ($i=0; $i<count($ti); $i++) {
            $out = $score[0]['ann']->calc($ti[$i])[0][0];

            if (($to[$i][0][0] == 1) && ($out <= 0.8)) $falseneg++;
            if (($to[$i][0][0] == 0) && ($out > 0.8)) $falsepos++;
        }

        $falsepos /= $totalham*1.0;
        $falseneg /= $totalspam*1.0;

        $fpstr = number_format($falsepos*100,2)."%";
        $fnstr = number_format($falseneg*100,2)."%";
        if (($falsepos < FPRATE) && ($falseneg < FNRATE)) {
            print("\n\nFOUND - FP = ".$fpstr.", FN = ".$fnstr."\n\n");
            $score[0]['ann']->wprint();
            $end = microtime(true);
            print("\n\nTime: ".($end-$start))."\n";

            for ($i = 0; isset($ti[$i]); $i++) {
                $s = $score[0]['ann']->calc($ti[$i])[0][0];
                if (($to[$i][0][0] == 1) && ($s <= 0.8)) echo "FN: #".$i."\t".$s." != ".$to[$i][0][0]."\n";
                if (($to[$i][0][0] == 0) && ($s > 0.8)) echo "FP: #".$i."\t".$s." != ".$to[$i][0][0]."\n";
            }
            exit(0);
        }

        echo "False Positive: ".$fpstr."\n";
        echo "False Negative: ".$fnstr."\n";

        for ($num=50; $num>0; $num--) {
            $str = "";
            for ($i=0; $i<count($ti); $i+=10) {
                $str .= ($score[0]['ann']->calc($ti[$i])[0][0] >= $num/50.0) ? "*" : " ";
            }
            echo $str."\n";
        }
        echo ob_get_flush();
    }

    $ann = [];

    # Top 10% + mutate
    for ($i=0; $i < POPULATION / 10; $i++) {
        $ann[] = clone $score[$i]['ann'];
        $ann[count($ann)-1]->mutate(100);

    }

    #crossing up to 50%
    while (count($ann) < POPULATION/2) {
        $a = $ann[rand(0,count($ann)-1)];
        $b = $ann[rand(0,count($ann)-1)];
        $a->cross($b);
        $a->mutate(100);
        $ann[] = clone $a;
    }

    #generate others
    while (count($ann) < POPULATION) {
        $ann[] = new ANN(INPUTS, LAYER,NEURON, 1);
        $ann[count($ann)-1]->random();
        $ann[count($ann)-1]->setRules($ti,$to);
    }

}
