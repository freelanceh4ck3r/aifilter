<?php

function randarr($y, $x) {
    for ($i=0; $i<$x; $i++)
        for ($j=0; $j<$y; $j++)
            $arr[$j][$i] = rand(-100000,100000) / 100000.0;
    return $arr;
}

function matmul($m1,$m2){
	$r=count($m1);
	$c=count($m2[0]);
	$p=count($m2);
	if(count($m1[0])!=$p) {
        throw new Exception('Incompatible matrixes');
    }
	$m3=array();
	for ($i=0;$i< $r;$i++){
		for($j=0;$j<$c;$j++){
			$m3[$i][$j]=0;
			for($k=0;$k<$p;$k++){
				$m3[$i][$j]+=$m1[$i][$k]*$m2[$k][$j];
			}
		}
	}
	return($m3);
}

function matminus($a,$b) {
    for ($i=0; $i < count($a); $i++)
        $c[][0] = $a[$i][0] - $b[$i][0];
    return $c;
}

function matnorm($a) {
    $sum=0;
    foreach ($a as $tmp) {
        if (is_array($tmp)) {
            foreach($tmp as $x) {
                $sum += pow($x,2);
            }
        } else {
            $sum += pow($tmp,2);
        }
    }
    return sqrt($sum);
}

class ANN {

    public $w = [];

    function __construct($ninput, $nlayers, $nneurons, $noutput) {
        $this->ni = $ninput;
        $this->nl = $nlayers;
        $this->nn = $nneurons;
        $this->no = $noutput;
        $this->w = [];
    }

    function random() {
        $this->w = [];
        for ($i=0; $i<$this->nl; $i++) {
            if ($i == 0)
                $this->w[] = randarr($this->nn, $this->ni);
            else
                $this->w[] = randarr($this->nn, $this->nn);
        }
        $this->w[] = randarr($this->no, $this->nn);
    }

    function sigmoid($x) {
        $x[0] = 1 / (1 + exp(-$x[0]));
        return $x;
    }

    function calc($input) {
        $layer = matmul($this->w[0], $input);

        for ($i=0; $i<$this->nl; $i++) {
            # add bias

            # run activation
            for ($j=0; $j < count($layer); $j++)
                $layer[$j] = $this->sigmoid($layer[$j]);

            $layer = matmul($this->w[$i+1], $layer);
        }

        $out = $layer;

        for ($j=0; $j<count($out); $j++)
            $out[$j] = $this->sigmoid($out[$j]);

        return $out;
    }

    function setRules($inp, $out) {
        $this->ti = $inp;
        $this->to = $out;
    }

    function test() {
        $a = [];
        for ($i=0; $i<count($this->ti); $i++) {
            $o = $this->calc($this->ti[$i]);
            $res = matminus($this->to[$i], $o);
            $a[] = matnorm($res);
        }
        return matnorm($a);
    }

    function cross($other) {
        for ($i=0; $i<count($this->w); $i++) {
            $tmp = $this->w[$i];
            $l = count($tmp);
            $l2 = count($tmp[0]);

            for ($j=0; $j<($l*$l2)/2; $j++) {
                $y = rand(0, $l-1);
                $x = rand(0, $l2-1);
                $this->w[$i][$y][$x] = $other->w[$i][$y][$x];
            }
        }
    }

    function mutate($maxstep) {
        for ($i=0; $i<count($this->w); $i++) {
            $tmp = $this->w[$i];
            $l = count($tmp);
            $l2 = count($tmp[0]);
            for ($j=0; $j<$l*$l2/2; $j++) {
                $y = rand(0, $l-1);
                $x = rand(0, $l2-1);
                $this->w[$i][$y][$x] = $this->w[$i][$y][$x] + ((rand(0, $maxstep*2)-$maxstep)/100.0);
            }
        }
    }

    function wprint() {
        echo (json_encode($this->w));
    }
}