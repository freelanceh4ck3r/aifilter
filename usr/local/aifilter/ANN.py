import numpy as np
import random
import math

class ANN:
    def __init__(self, ninput, nlayers, nneurons, noutput):
        self.ni = ninput
        self.nl = nlayers
        self.nn = nneurons
        self.no = noutput
        self.w = []

    def random(self):
        for i in range(self.nl):
            if i == 0:
                self.w.append(np.random.random((self.nn, self.ni))*2-1)
            else:
                self.w.append(np.random.random((self.nn, self.nn))*2-1)
        self.w.append(np.random.random((self.no, self.nn))*2-1)

    def sigmoid(self, x):
        return 1 / (1 + math.exp(-x))

    def calc(self, input):
        layer = np.matmul(self.w[0], input)


        for i in range(self.nl):
            # add bias

            # run activation
            for j in range(len(layer)):
                #print(j)
                layer[j] = self.sigmoid(layer[j])

            layer = np.matmul(self.w[i+1], layer)


        out = layer

        for j in range(len(out)):
            out[j] = self.sigmoid(out[j])

        return out

    def setRules(self,inp,out):
        self.ti = inp
        self.to = out

    def test(self):
        ok=0.0
        a = []
        for i in range(len(self.ti)):
            o = self.calc(self.ti[i])
            #print(o)
            #print("---")
            #print(self.to[i])
            res = self.to[i] - o
            a.append(np.linalg.norm(res))
            #ok += np.linalg.norm(res)

        #print(a)
        #exit(0)
        return np.linalg.norm(np.array(a))

    def cross(self, other):
        for i in range(len(self.w)):
            tmp = self.w[i]
            l = len(tmp)
            l2 = len(tmp[0])
            for j in range((l*l2)//2):
                y = random.randrange(l)
                x = random.randrange(l2)
                self.w[i][y][x] = other.w[i][y][x]

    def mutate(self, maxstep):
        for i in range(len(self.w)):
            tmp = self.w[i]
            l = len(tmp)
            l2 = len(tmp[0])
            for j in range((l*l2)//2):
                y = random.randrange(l)
                x = random.randrange(l2)
                self.w[i][y][x] = self.w[i][y][x] + ((random.randrange(maxstep*2)-maxstep)/100)

    def print(self):
        print(self.w)

    def setw(self, weights):
        #np.array()
        self.w = weights
